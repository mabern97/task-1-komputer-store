import API from "./JS/Systems/ApiAccess.js";
import Device from './JS/Systems/Device.js';
import UserStore from './JS/Systems/UserStore.js';
//import InventoryStore from './Systems/InventoryStore.js';

export default class App {
    constructor() {
        // API Access
        this.API = new API();

        // Devices
        this.user = new UserStore();
        this.products = [];
        this.selectedDevice = null;

        // Card DOM Elements
        this.elCardUser = document.getElementById("user-info-card");
        this.elCardWork = document.getElementById("user-work-card");

        // Product DOM Elements
        this.elDeviceList = document.getElementById("inputGroupSelect01");
        this.elDeviceName = document.getElementById("device-name");
        this.elDeviceDetails = document.getElementById("device-specs");
        this.elDeviceImage = document.getElementById("device-image");
        this.elDevicePrice = document.getElementById("device-price");

        // User DOM Elements
        this.elUserLoanBalance = document.getElementById("user-loan-balance");
        this.elUserBalance = document.getElementById("user-balance");
        this.elUserIncome = document.getElementById("user-income");

        // Button actions
        this.elActionGetLoan = document.getElementById("action_loan");
        this.elActionDeposit = document.getElementById("action_bank");
        this.elActionWork = document.getElementById("action_work");
        this.elActionBuy = document.getElementById("action_buy");

        this.elActionGetLoan.onclick = () => {
            this.user.RequestLoan(() => {
                this.Render();
            });
        }

        this.elActionDeposit.onclick = () => {
            this.user.DepositIncome(_ => {
                this.Render();
            });
        }

        this.elActionWork.onclick = () => {
            this.user.Work(() => {
                this.Render();
            })
        }

        this.elActionBuy.onclick = () => {
            this.OnBuyDevice();
        }

        this.Populate(() => {
            this.Render()

            this.elDeviceList.onchange = (e) => {
                this.OnDeviceSelected(e.target.value);
            };
        });
    }

    /*
        Populate the application (retrieve all the relevant data from our API)
    */
    async Populate(callback) {
        // Retrieve User Details

        // Retrieve Procucts
        await this.API.retrieveProducts()
        .then(products => products.forEach(product => {
            //this.products.push(product)
            let device = new Device();
            device.Id = product.id;
            device.Name = product.name;
            device.Price = product.price;

            this.products.push(device);
        }))
        .then(() => {
            callback !== undefined && callback();
        })
    }

    Render() {
        // Add each product to a list
        this.products.forEach(product => this.AppendDevice(product));

        // Populate User Info
        this.elUserBalance.innerText = `Account Balance: ${this.user.GetBalance()} kr`;
        this.elUserLoanBalance.innerText = `Loan Balance: ${this.user.GetLoanBalance()} kr`;
        this.elUserIncome.innerText = `Income Earned: ${this.user.GetIncome()} kr`;
    }

    AppendDevice(device) {
        let elDeviceOption = document.createElement('option');
        let {Id, Name, Price} = device;
        elDeviceOption.innerText = `${Name}: -,${Price}`;
        elDeviceOption.setAttribute("value", Id);

        this.elDeviceList.appendChild(elDeviceOption);
    }

    OnBuyDevice() {
        let deviceId = this.selectedDevice;

        if (deviceId == null)
        {
            return;
        }

        let device = this.products[deviceId];
        let {Name, Price} = device;

        let loanBalance = this.user.GetLoanBalance();
        let usedLoanCost = 0;
        let finalPrice = Price;

        let usedLoan = false;
        if (this.user.hasLoan && Price < loanBalance) {
            usedLoanCost = loanBalance - Price;
            finalPrice -= loanBalance;
            if (finalPrice < 0)
            {
                finalPrice = 0;
            }

            usedLoan = true;
        }

        // Deny purchase if insufficient funds
        if (!usedLoan && !this.user.CanAmountBeSpent(finalPrice))
        {
            alert("Unable to complete your purchase, you do not have sufficient funds in your account.")
            return;
        }

        // Ask for purchase confirmation
        if (confirm(`Do you wish to purchase a ${Name} for ${finalPrice}? ${usedLoan ? `(Your loan of ${loanBalance} will be used as part of the purchase cost)` : ""}`))
        {
            if (usedLoan)
            {
                let loanUse = loanBalance - usedLoanCost;
                this.user.Purchase(finalPrice, loanUse);
            } else {
                this.user.Purchase(finalPrice, 0);
            }

            this.Render();
            alert(`Congratulations, you have purchased a ${Name} for ${finalPrice}!`);
        }
    }

    // List item selected
    async OnDeviceSelected(deviceId) {
        await this.API.retrieveProduct(deviceId)
        .then(response => {
            let {name, image, features, price} = response;

            // Set selected device
            this.selectedDevice = deviceId;

            // Set Device Name
            this.elDeviceName.innerText = name;
            
            // Remove previous device image(s)
            while (this.elDeviceImage.firstChild) {
                this.elDeviceImage.removeChild(this.elDeviceImage.firstChild);
            }

            // Load Device Image
            let img = new Image();
            img.className += "card-img";
            img.src = image;
            img.onload = () => {
                this.elDeviceImage.appendChild(img);
            };

            // Set Device Specs
                // Remove previous specifications
            while (this.elDeviceDetails.firstChild) {
                this.elDeviceDetails.removeChild(this.elDeviceDetails.firstChild);
            }
                // Add new specifications
            let specTitle = document.createElement('h5');
            specTitle.innerText = "Specifications";

            let specElements = document.createElement('ul');
            specElements.className += "list-group list-group-flush";
            
            for (const [key, value] of Object.entries(features)) {
                let specItem = document.createElement('li');
                specItem.className += "list-group-item";

                let specItemTitle = document.createElement('h6');
                specItemTitle.innerText = key;
                specItem.appendChild(specItemTitle);

                if (typeof value == "object") {
                    for (const [innerKey, innerValue] of Object.entries(value)) {
                        let innerSpecItemDetail = document.createElement('p');
                        innerSpecItemDetail.innerText = innerKey;

                        let innerSpecList = document.createElement('ul');
                        innerSpecList.className += "list-group";

                        innerValue.forEach(featureValue => {
                            let innerSpecListItem = document.createElement('li');
                            innerSpecListItem.className += "list-group-item";
                            innerSpecListItem.innerText = featureValue;

                            innerSpecList.appendChild(innerSpecListItem);
                        });

                        innerSpecItemDetail.appendChild(innerSpecList);
                        specItem.appendChild(innerSpecItemDetail);
                    }
                } else {
                    let specItemDetail = document.createElement('p');
                    specItemDetail.innerText = value;
                    specItem.appendChild(specItemDetail);
                }

                specElements.appendChild(specItem);
            }
            
            this.elDeviceDetails.appendChild(specTitle);
            this.elDeviceDetails.appendChild(specElements);

            // Set Device Price
            this.elDevicePrice.innerText = `Price: kr ${price}`;
        });
    }
}

new App();