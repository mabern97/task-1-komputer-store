/*
    Device class

    This class is a representation of a device, the basic properties of a device
    include:
        - Manufactrurer (ie. the vendor)
        - Features (ie. what it can do)
        - Specification (ie. processor type, gpu type, RAM, etc.)
*/

export default class Device {
    constructor() {
        this.Id = 0
        this.Name = ""
        this.Price = 0
        this.Image = ""
        this.Features = {
            /*"Processor": "",
            "Storage": "",
            "RAM": "",*/
        }
    }

    // Methods
    GetFeatureSet() {
        return this.Features;
    }

    GetFeatureCount() {
        return Object.keys(this.Features).length;
    }

    ClearFeatureSet() {
        this.Features = {};
    }

    AddFeature(name, information)
    {
        this.Features[name] = information;
    }

    RemoveFeature(name)
    {
        //this.Features[name] = null;
        delete this.Features[name];
    }
}