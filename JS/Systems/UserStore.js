/*
    User class

    this class contains the implementation of a user account.
*/

export default class UserStore {
    constructor() {
        this.bankBalance = 0;
        this.workIncome = 0;
        this.loan = 25000;

        this.hasLoan = true;

        this.firstName = "Mathias";
        this.lastName = "Berntsen";
    }

    GetName = () => `${this.firstName} ${this.lastName}`;

    GetBalance = () => this.bankBalance;

    GetIncome = () => this.workIncome;

    DepositIncome = (callback) => {
        this.bankBalance += this.workIncome;
        this.workIncome = 0;

        callback();
    }

    Work = (callback) => {
        this.workIncome += 100;
        callback();
    }

    GetLoanBalance = () => this.loan;

    RequestLoan(callback) {
        if (this.hasLoan) {
            alert("Unable to request a new loan, you already have a loan. Please pay off your loan before requesting another loan.");
            return;
        }

        let maxAmount = this.bankBalance * 2;
        let amount = prompt(`Please enter the amount you wish to loan? (Maximum amount is ${maxAmount}kr)`);
        amount = parseInt(amount);
        
        if (!isNaN(amount) && amount > 0)
        {
            if (amount > maxAmount) {
                alert(`We cannot approve your requested loan amount of ${amount}kr, we will only allocate a maximum of ${maxAmount}kr (Exceeded by ${amount - maxAmount}kr)`);
                return;
            }

            this.loan += amount;
            this.hasLoan = true;
            callback();
        }
        else
        {
            alert("Error! The value you have entered is invalid. Failed to process loan request.");
        }
    }

    CanAmountBeSpent(amount) {
        return this.bankBalance >= amount;
    }

    Purchase(wallet, loan)
    {
        this.bankBalance -= wallet;
        this.loan -= loan;
        if (this.loan == 0)
        {
            this.hasLoan = false;
        }
    }
}