/*export class API {

}*/

export default class API {
    constructor()
    {
        //this.api = "http://localhost:5000/api"
        this.api = "http://mathiastb.no:1929/api"
    }

    async retrieveProducts() {
        return await fetch(`${this.api}/product`)
        .then(response => response.json())
    }

    async retrieveProduct(id) {
        return await fetch(`${this.api}/product/${id}/details`)
        .then(response => response.json())
    }
}