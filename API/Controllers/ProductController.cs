using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using KomputerStoreAPI.Context;
using KomputerStoreAPI.Models;
using Newtonsoft.Json;
using AutoMapper;

using KomputerStoreAPI.DTOs;

namespace KomputerStoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly StoreContext _context;
        private readonly IMapper _mapper;

        public ProductController(StoreContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Product
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductShortDTO>>> GetProducts()
        {
            IEnumerable<Product> products = await _context.Products.ToListAsync();
            IEnumerable<ProductShortDTO> productsDTO = _mapper.Map<IEnumerable<ProductShortDTO>>(products);

            return Ok(productsDTO);
        }

        // GET: api/Product/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductShortDTO>> GetProduct(int id)
        {
            Product product = await _context.Products.FindAsync(id);
            product.Features = JsonConvert.DeserializeObject<Dictionary<string, object>>(product.FeatureSet);

            if (product == null)
            {
                return NotFound();
            }

            ProductShortDTO productDTO = _mapper.Map<ProductShortDTO>(product);

            return productDTO;
        }

        // GET: api/Product/5/Details
        [HttpGet("{id}/Details")]
        public async Task<ActionResult<ProductDTO>> GetProductDetails(int id)
        {
            Product product = await _context.Products.FindAsync(id);
            product.Features = JsonConvert.DeserializeObject<Dictionary<string, object>>(product.FeatureSet);

            if (product == null)
            {
                return NotFound();
            }

            ProductDTO productDTO = _mapper.Map<ProductDTO>(product);

            return productDTO;
        }

        // PUT: api/Product/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(int id, Product product)
        {
            if (id != product.Id)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Product
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct(Product product)
        {
            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduct", new { id = product.Id }, product);
        }

        // DELETE: api/Product/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Product>> DeleteProduct(int id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return product;
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}
