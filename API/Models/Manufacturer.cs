﻿using System;
using System.Collections.Generic;

namespace KomputerStoreAPI.Models
{
    public class Manufacturer
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<ProductManufacturerCatalog> Products { get; set; }
    }
}
