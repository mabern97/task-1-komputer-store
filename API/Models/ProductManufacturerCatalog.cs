﻿using System;
namespace KomputerStoreAPI.Models
{
    public class ProductManufacturerCatalog
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int ManufacturerId { get; set; }
        public Manufacturer Manufacturer { get; set; }
    }
}
