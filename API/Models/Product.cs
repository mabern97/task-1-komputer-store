﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace KomputerStoreAPI.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FeatureSet { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }

        [NotMapped]
        public Dictionary<string, object> Features { get; set; }
        public IEnumerable<ProductManufacturerCatalog> Manufacturers;

        public Product()
        {
            //Features = JsonConvert.DeserializeObject<Dictionary<string, object>>(FeatureSet);
        }
    }
}
