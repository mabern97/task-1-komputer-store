﻿using System;
using Microsoft.AspNetCore;
using Microsoft.EntityFrameworkCore;
using KomputerStoreAPI.Models;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace KomputerStoreAPI.Context
{
    public class StoreContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<ProductManufacturerCatalog> Catalogs { get; set; }

        public StoreContext(DbContextOptions<StoreContext> options)
            :base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Compusite key
            modelBuilder.Entity<ProductManufacturerCatalog>().HasKey(me => new { me.ManufacturerId, me.ProductId});

            // Seeding
            Manufacturer manufacturer = new Manufacturer();
            manufacturer.Id = 1;
            manufacturer.Name = "Apple Inc.";

            #region Mac Products
            List<string> chargingSupport = new List<string>();
            chargingSupport.Add("Charging");
            chargingSupport.Add("DisplayPort");
            chargingSupport.Add("Thunderbolt (up to 40Gb/s");
            chargingSupport.Add("USB 3.1 Gen 2 (up to 10Gb/s");

            // Macbook Pro 13-inch (base)
            Product productA = new Product();
            productA.Id = 1;
            productA.Name = "Macbook Pro 13-inch";
            productA.Price = 16790.00M;
            productA.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/mbp13touch-space-select-202005?wid=904&hei=840&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1587460552755";

            Dictionary<string, object> featuresA = new Dictionary<string, object>();
            featuresA.Add("Processor", "1.4 GHz 4-core Intel Core i5-processor (8th gen.)");
            featuresA.Add("Graphics", "Intel Iris Plus Graphics 645");
            featuresA.Add("Memory", "8 GB 2133 MHz LPDDR3 memory");
            featuresA.Add("Storage", "256GB SSD");
            featuresA.Add("Display", "13.3-inch Retina-screen with True Tone");
            featuresA.Add("Keyboard", "Magic Keyboard, Touch Bar, Touch ID");
            featuresA.Add("Trackpad", "Magic Trackpad");

            Dictionary<string, object> chargingExpansionA = new Dictionary<string, object>();
            chargingExpansionA.Add("Two Thunderbolt 3 (USB-C) ports", chargingSupport);
            featuresA.Add("Charging and Expansion", chargingExpansionA);

            string featureSetA = JsonConvert.SerializeObject(featuresA);
            productA.FeatureSet = featureSetA;

            // Macbook Pro 13-inch (base 2)
            Product productB = new Product();
            productB.Id = 2;
            productB.Name = "Macbook Pro 13-inch";
            productB.Price = 19290.00M;
            productB.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/mbp13touch-space-select-202005?wid=904&hei=840&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1587460552755";

            Dictionary<string, object> featuresB = new Dictionary<string, object>();
            featuresB.Add("Processor", "1.4 GHz 4-core Intel Core i5-processor (8th gen.)");
            featuresB.Add("Graphics", "Intel Iris Plus Graphics 645");
            featuresB.Add("Memory", "8 GB 2133 MHz LPDDR3 memory");
            featuresB.Add("Storage", "512GB SSD");
            featuresB.Add("Display", "13.3-inch Retina-screen with True Tone");
            featuresB.Add("Keyboard", "Magic Keyboard, Touch Bar, Touch ID");
            featuresB.Add("Trackpad", "Magic Trackpad");

            Dictionary<string, object> chargingExpansionB = new Dictionary<string, object>();
            chargingExpansionB.Add("Two Thunderbolt 3 (USB-C) ports", chargingSupport);
            featuresB.Add("Charging and Expansion", chargingExpansionB);

            string featureSetB = JsonConvert.SerializeObject(featuresB);
            productB.FeatureSet = featureSetB;

            // Macbook Pro 13-inch (base 3)
            Product productC = new Product();
            productC.Id = 3;
            productC.Name = "Macbook Pro 13-inch";
            productC.Price = 24990.00M;
            productC.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/mbp13touch-space-select-202005?wid=904&hei=840&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1587460552755";

            Dictionary<string, object> featuresC = new Dictionary<string, object>();
            featuresC.Add("Processor", "2.0 GHz 4-core Intel Core i5-processor (10th gen.)");
            featuresC.Add("Graphics", "Intel Iris Plus Graphics");
            featuresC.Add("Memory", "16 GB 3733 MHz LPDDR4X memory");
            featuresC.Add("Storage", "512GB SSD");
            featuresC.Add("Display", "13.3-inch Retina-screen with True Tone");
            featuresC.Add("Keyboard", "Magic Keyboard, Touch Bar, Touch ID");
            featuresC.Add("Trackpad", "Magic Trackpad");

            Dictionary<string, object> chargingExpansionC = new Dictionary<string, object>();
            chargingExpansionC.Add("Four Thunderbolt 3 (USB-C) ports", chargingSupport);
            featuresC.Add("Charging and Expansion", chargingExpansionC);

            string featureSetC = JsonConvert.SerializeObject(featuresC);
            productC.FeatureSet = featureSetC;

            // Macbook Pro 16-inch (base)
            Product productD = new Product();
            productD.Id = 4;
            productD.Name = "Macbook Pro 16-inch";
            productD.Price = 28990.00M;
            productD.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/mbp16touch-space-select-201911?wid=904&hei=840&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1572825197207";

            Dictionary<string, object> featuresD = new Dictionary<string, object>();
            featuresD.Add("Processor", "2.6 GHz 6-core Intel Core i7-processor (9th gen.)");
            featuresD.Add("Graphics", "AMD Radeon Pro 5300M Graphics with 4GB GDDR6 memory");
            featuresD.Add("Memory", "16 GB 3733 MHz LPDDR4X memory");
            featuresD.Add("Storage", "512GB SSD");
            featuresD.Add("Display", "16-inch Retina-screen with True Tone");
            featuresD.Add("Keyboard", "Magic Keyboard, Touch Bar, Touch ID");
            featuresD.Add("Trackpad", "Magic Trackpad");

            Dictionary<string, object> chargingExpansionD = new Dictionary<string, object>();
            chargingExpansionD.Add("Four Thunderbolt 3 (USB-C) ports", chargingSupport);
            featuresD.Add("Charging and Expansion", chargingExpansionD);

            string featureSetD = JsonConvert.SerializeObject(featuresD);
            productD.FeatureSet = featureSetD;

            // Macbook Pro 16-inch (base 2)
            Product productE = new Product();
            productE.Id = 5;
            productE.Name = "Macbook Pro 16-inch";
            productE.Price = 33990.00M;
            productE.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/mbp16touch-space-select-201911?wid=904&hei=840&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1572825197207";

            Dictionary<string, object> featuresE = new Dictionary<string, object>();
            featuresE.Add("Processor", "2.8 GHz 8-core Intel Core i9-processor (9th gen.)");
            featuresE.Add("Graphics", "AMD Radeon Pro 5300M Graphics with 4GB GDDR6 memory");
            featuresE.Add("Memory", "16 GB 3733 MHz LPDDR4X memory");
            featuresE.Add("Storage", "1TB SSD");
            featuresE.Add("Display", "16-inch Retina-screen with True Tone");
            featuresE.Add("Keyboard", "Magic Keyboard, Touch Bar, Touch ID");
            featuresE.Add("Trackpad", "Magic Trackpad");

            Dictionary<string, object> chargingExpansionE = new Dictionary<string, object>();
            chargingExpansionE.Add("Four Thunderbolt 3 (USB-C) ports", chargingSupport);
            featuresE.Add("Charging and Expansion", chargingExpansionE);

            string featureSetE = JsonConvert.SerializeObject(featuresE);
            productE.FeatureSet = featureSetE;
            #endregion

            #region iPhone Products

            Product iPhoneSE_A = new Product();
            iPhoneSE_A.Id = 6;
            iPhoneSE_A.Name = "iPhone SE 64GB";
            iPhoneSE_A.Price = 5490.00M;
            iPhoneSE_A.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-se-white-select-2020_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1586574260599";

            Product iPhoneSE_B = new Product();
            iPhoneSE_B.Id = 7;
            iPhoneSE_B.Name = "iPhone SE 128GB";
            iPhoneSE_B.Price = 6190.00M;
            iPhoneSE_B.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-se-black-select-2020_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1586574259781";

            Product iPhoneSE_C = new Product();
            iPhoneSE_C.Id = 8;
            iPhoneSE_C.Name = "iPhone SE 256GB";
            iPhoneSE_C.Price = 7690.00M;
            iPhoneSE_C.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-se-red-select-2020_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1586574260374";

            Product iPhone11_A = new Product();
            iPhone11_A.Id = 9;
            iPhone11_A.Name = "iPhone 11 64GB";
            iPhone11_A.Price = 8490.00M;
            iPhone11_A.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone11-white-select-2019_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1567021770073";

            Product iPhone11_B = new Product();
            iPhone11_B.Id = 10;
            iPhone11_B.Name = "iPhone 11 128GB";
            iPhone11_B.Price = 9090.00M;
            iPhone11_B.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone11-black-select-2019_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1567021766023";

            Product iPhone11_C = new Product();
            iPhone11_C.Id = 11;
            iPhone11_C.Name = "iPhone 11 256GB";
            iPhone11_C.Price = 10390.00M;
            iPhone11_C.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone11-green-select-2019_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1567021766404";

            Product iPhone11Pro_A = new Product();
            iPhone11Pro_A.Id = 12;
            iPhone11Pro_A.Name = "iPhone 11 Pro 64GB";
            iPhone11Pro_A.Price = 11990.00M;
            iPhone11Pro_A.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-11-pro-space-select-2019_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1567808552932";

            Product iPhone11Pro_B = new Product();
            iPhone11Pro_B.Id = 13;
            iPhone11Pro_B.Name = "iPhone 11 Pro 256GB";
            iPhone11Pro_B.Price = 13890.00M;
            iPhone11Pro_B.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-11-pro-silver-select-2019_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1567808551667";

            Product iPhone11Pro_C = new Product();
            iPhone11Pro_C.Id = 14;
            iPhone11Pro_C.Name = "iPhone 11 Pro 512GB";
            iPhone11Pro_C.Price = 16190.00M;
            iPhone11Pro_C.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-11-pro-midnight-green-select-2019_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1567808549969";

            Product iPhone11ProMax_A = new Product();
            iPhone11ProMax_A.Id = 15;
            iPhone11ProMax_A.Name = "iPhone 11 Pro Max 64GB";
            iPhone11ProMax_A.Price = 12990.00M;
            iPhone11ProMax_A.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-11-pro-max-space-select-2019_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1567808546844";

            Product iPhone11ProMax_B = new Product();
            iPhone11ProMax_B.Id = 16;
            iPhone11ProMax_B.Name = "iPhone 11 Pro Max 256GB";
            iPhone11ProMax_B.Price = 14890.00M;
            iPhone11ProMax_B.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-11-pro-max-midnight-green-select-2019_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1567808545922";

            Product iPhone11ProMax_C = new Product();
            iPhone11ProMax_C.Id = 17;
            iPhone11ProMax_C.Name = "iPhone 11 Pro Max 512GB";
            iPhone11ProMax_C.Price = 17190.00M;
            iPhone11ProMax_C.Image = "https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-11-pro-max-gold-select-2019_GEO_EMEA?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1567808544078";

            Dictionary<string, object> iPhoneSE_Features = new Dictionary<string, object>();
            Dictionary<string, object> iPhone11_Features = new Dictionary<string, object>();
            Dictionary<string, object> iPhone11Pro_Features = new Dictionary<string, object>();
            Dictionary<string, object> iPhone11ProMax_Features = new Dictionary<string, object>();

            List<string> sizeWeightSE = new List<string>();
            sizeWeightSE.Add("Height: 138.4 mm");
            sizeWeightSE.Add("Width: 67.3 mm");
            sizeWeightSE.Add("Depth: 7.3 mm");
            sizeWeightSE.Add("Weight: 148 grams");

            List<string> sizeWeight11 = new List<string>();
            sizeWeight11.Add("Height: 150.9 mm");
            sizeWeight11.Add("Width: 75.7 mm");
            sizeWeight11.Add("Depth: 8.3 mm");
            sizeWeight11.Add("Weight: 194 grams");

            List<string> sizeWeight11Pro = new List<string>();
            sizeWeight11Pro.Add("Height: 144.0 mm");
            sizeWeight11Pro.Add("Width: 71.4 mm");
            sizeWeight11Pro.Add("Depth: 8.1 mm");
            sizeWeight11Pro.Add("Weight: 188 grams");

            List<string> sizeWeight11ProMax = new List<string>();
            sizeWeight11ProMax.Add("Height: 158.0 mm");
            sizeWeight11ProMax.Add("Width: 77.8 mm");
            sizeWeight11ProMax.Add("Depth: 8.1 mm");
            sizeWeight11ProMax.Add("Weight: 226 grams");

            List<string> displaySE = new List<string>();
            displaySE.Add("Retina HD display");
            displaySE.Add("4.7-inch widescreen LCD Multi-Touch display with IPS technology");
            displaySE.Add("1334-by-750 pixel resolution at 326 ppi");
            displaySE.Add("1400:1 constrast ratio");
            displaySE.Add("True Tone display");
            displaySE.Add("Wide color display (P3)");
            displaySE.Add("Haptic Touch");
            displaySE.Add("625 nits max brightness");
            displaySE.Add("Fingerprint-resistant olephobic coating");
            displaySE.Add("Display Zoom");
            displaySE.Add("Reachability");

            List<string> display11 = new List<string>();
            display11.Add("Liquid Retina HD display");
            display11.Add("6.1-inch all-screen LCD Multi-Touch display with IPS technology");
            display11.Add("1792-by-828 pixel resolution at 326 ppi");
            display11.Add("1400:1 constrast ratio");
            display11.Add("True Tone display");
            display11.Add("Wide color display (P3)");
            display11.Add("Haptic Touch");
            display11.Add("625 nits max brightness");
            display11.Add("Fingerprint-resistant olephobic coating");

            List<string> display11Pro = new List<string>();
            display11Pro.Add("Super Retina XDR display");
            display11Pro.Add("5.8-inch all-screen OLED Multi-Touch display");
            display11Pro.Add("HDR display");
            display11Pro.Add("2436-by-1125 pixel resolution at 458 ppi");
            display11Pro.Add("2,000,000:1 constrast ratio");
            display11Pro.Add("True Tone display");
            display11Pro.Add("Wide color display (P3)");
            display11Pro.Add("Haptic Touch");
            display11Pro.Add("800 nits max brightness; 1200 nits max brightness (HDR)");
            display11Pro.Add("Fingerprint-resistant olephobic coating");

            List<string> display11ProMax = new List<string>();
            display11ProMax.Add("Super Retina XDR display");
            display11ProMax.Add("6.5-inch all-screen OLED Multi-Touch display");
            display11ProMax.Add("HDR display");
            display11ProMax.Add("2688-by-1242 pixel resolution at 458 ppi");
            display11ProMax.Add("2,000,000:1 constrast ratio");
            display11ProMax.Add("True Tone display");
            display11ProMax.Add("Wide color display (P3)");
            display11ProMax.Add("Haptic Touch");
            display11ProMax.Add("800 nits max brightness; 1200 nits max brightness (HDR)");
            display11ProMax.Add("Fingerprint-resistant olephobic coating");

            List<string> cameraSE = new List<string>();
            cameraSE.Add("12MP Wide camera");
            cameraSE.Add("ƒ / 1.8 aperture");
            cameraSE.Add("Digital zoom up to 5x");
            cameraSE.Add("Portrait mode with advanced bokeh and Depth Control");
            cameraSE.Add("Portrait Lighting with six effects (Natural Studio, Contour, Stage, Stage Mono, High-Key Mono)");
            cameraSE.Add("Optical image stabilization");
            cameraSE.Add("Six-element lens");
            cameraSE.Add("LED True Tone flash with Slow Sync");
            cameraSE.Add("Panorama (up to 63MP)");
            cameraSE.Add("Sapphire crystal lens cover");
            cameraSE.Add("Autofocus with Focus Pixels");
            cameraSE.Add("Wide color capture for photos and Live Photos");
            cameraSE.Add("Next-generation Smart HDR for photos");
            cameraSE.Add("Advanced red-eye correction");
            cameraSE.Add("Auto image stabilization");
            cameraSE.Add("Burst mode");
            cameraSE.Add("Photo geotagging");
            cameraSE.Add("Image formats captured: HEIF and JPEG");

            List<string> camera11 = new List<string>();
            camera11.Add("Dual 12MP Ultra Wide and Wide camera");
            camera11.Add("Ultra Wide: ƒ / 2.4 aperture and 120° field of view");
            camera11.Add("Wide: ƒ / 1.8 aperture");
            camera11.Add("2x optical zoom out; digital zoom up to 5x");
            camera11.Add("Portrait mode with advanced bokeh and Depth Control");
            camera11.Add("Portrait Lighting with six effects (Natural Studio, Contour, Stage, Stage Mono, High-Key Mono)");
            camera11.Add("Optical image stabilization (Wide)");
            camera11.Add("Five-element lens (Ultra Wide); six-element lens (Wide)");
            camera11.Add("Brighter True Tone flash with Slow Sync");
            camera11.Add("Panorama (up to 63MP)");
            camera11.Add("Sapphire crystal lens cover");
            camera11.Add("100% Focus Pixels (Wide)");
            camera11.Add("Night mode");
            camera11.Add("Auto Adjustments");
            camera11.Add("Next-generation Smart HDR for photos");
            camera11.Add("Wide color capture for photos and Live Photos");
            camera11.Add("Advanced red-eye correction");
            camera11.Add("Auto image stabilization");
            camera11.Add("Burst mode");
            camera11.Add("Photo geotagging");
            camera11.Add("Image formats captured: HEIF and JPEG");

            List<string> camera11Pro = new List<string>();
            camera11.Add("Triple 12MP Ultra Wide, Wide and Telephoto camera");
            camera11.Add("Ultra Wide: ƒ / 2.4 aperture and 120° field of view");
            camera11.Add("Wide: ƒ / 1.8 aperture");
            camera11.Add("2x optical zoom in, 2x optical zoom out; digital zoom up to 10x");
            camera11.Add("Portrait mode with advanced bokeh and Depth Control");
            camera11.Add("Portrait Lighting with six effects (Natural Studio, Contour, Stage, Stage Mono, High-Key Mono)");
            camera11.Add("Dual optical image stabilization (Wide and Telephoto)");
            camera11.Add("Five-element lens (Ultra Wide); six-element lens (Wide and Telephoto)");
            camera11.Add("Brighter True Tone flash with Slow Sync");
            camera11.Add("Panorama (up to 63MP)");
            camera11.Add("Sapphire crystal lens cover");
            camera11.Add("100% Focus Pixels (Wide)");
            camera11.Add("Night mode");
            camera11.Add("Auto Adjustments");
            camera11.Add("Next-generation Smart HDR for photos");
            camera11.Add("Wide color capture for photos and Live Photos");
            camera11.Add("Advanced red-eye correction");
            camera11.Add("Auto image stabilization");
            camera11.Add("Burst mode");
            camera11.Add("Photo geotagging");
            camera11.Add("Image formats captured: HEIF and JPEG");

            List<string> videoSE = new List<string>();
            videoSE.Add("4K video recording at 24 fps, 30 fps, or 60 fps");
            videoSE.Add("1080p HD video recording at 30 fps or 60 fps");
            videoSE.Add("720p HD video recording at 30 fps");
            videoSE.Add("Extended dynamic range for video up to 30 fps");
            videoSE.Add("Optical image stabilization for video");
            videoSE.Add("Digital zoom up to 3x");
            videoSE.Add("LED True Tone flash");
            videoSE.Add("QuickTake video");
            videoSE.Add("Slo‑mo video support for 1080p at 120 fps or 240 fps");
            videoSE.Add("Time‑lapse video with stabilization");
            videoSE.Add("Cinematic video stabilization(4K, 1080p, and 720p)");
            videoSE.Add("Continuous autofocus video");
            videoSE.Add("Take 8MP still photos while recording 4K video");
            videoSE.Add("Playback zoom");
            videoSE.Add("Video formats recorded: HEVC and H.264");
            videoSE.Add("Stereo recording");

            List<string> video11 = new List<string>();
            video11.Add("4K video recording at 24 fps, 30 fps, or 60 fps");
            video11.Add("1080p HD video recording at 30 fps or 60 fps");
            video11.Add("720p HD video recording at 30 fps");
            video11.Add("Extended dynamic range for video up to 30 fps");
            video11.Add("Optical image stabilization for video (Wide)");
            video11.Add("2x optical zoom out; digital zoom up to 3x");
            video11.Add("Audio zoom");
            video11.Add("Brighter True Tone flash");
            video11.Add("QuickTake video with subject tracking");
            video11.Add("Slo‑mo video support for 1080p at 120 fps or 240 fps");
            video11.Add("Time‑lapse video with stabilization");
            video11.Add("Cinematic video stabilization(4K, 1080p, and 720p)");
            video11.Add("Continuous autofocus video");
            video11.Add("Take 8MP still photos while recording 4K video");
            video11.Add("Playback zoom");
            video11.Add("Video formats recorded: HEVC and H.264");
            video11.Add("Stereo recording");

            List<string> video11Pro = new List<string>();
            video11Pro.Add("4K video recording at 24 fps, 30 fps, or 60 fps");
            video11Pro.Add("1080p HD video recording at 30 fps or 60 fps");
            video11Pro.Add("720p HD video recording at 30 fps");
            video11Pro.Add("Extended dynamic range for video up to 60 fps");
            video11Pro.Add("Optical image stabilization for video (Wide and Telephoto)");
            video11Pro.Add("2x optical zoom in, 2x optical zoom out; digital zoom up to 6x");
            video11Pro.Add("Audio zoom");
            video11Pro.Add("Brighter True Tone flash");
            video11Pro.Add("QuickTake video with subject tracking");
            video11Pro.Add("Slo‑mo video support for 1080p at 120 fps or 240 fps");
            video11Pro.Add("Time‑lapse video with stabilization");
            video11Pro.Add("Cinematic video stabilization(4K, 1080p, and 720p)");
            video11Pro.Add("Continuous autofocus video");
            video11Pro.Add("Take 8MP still photos while recording 4K video");
            video11Pro.Add("Playback zoom");
            video11Pro.Add("Video formats recorded: HEVC and H.264");
            video11Pro.Add("Stereo recording");

            List<string> frontCameraSE = new List<string>();
            frontCameraSE.Add("7MP camera");
            frontCameraSE.Add("ƒ / 2.2 aperture");
            frontCameraSE.Add("Portrait mode with advanced bokeh and Depth Control");
            frontCameraSE.Add("Portrait Lighting with six effects(Natural, Studio, Contour, Stage, Stage Mono, High-Key Mono)");
            frontCameraSE.Add("1080p HD video recording at 30 fps");
            frontCameraSE.Add("Retina Flash");
            frontCameraSE.Add("QuickTake video");
            frontCameraSE.Add("Wide color capture for photos and Live Photos");
            frontCameraSE.Add("Auto HDR for photos");
            frontCameraSE.Add("Auto image stabilization");
            frontCameraSE.Add("Burst mode");
            frontCameraSE.Add("Cinematic video stabilization(1080p and 720p)");

            List<string> frontCamera11 = new List<string>();
            frontCamera11.Add("12MP camera");
            frontCamera11.Add("ƒ / 2.2 aperture");
            frontCamera11.Add("Portrait mode with advanced bokeh and Depth Control");
            frontCamera11.Add("Portrait Lighting with six effects(Natural, Studio, Contour, Stage, Stage Mono, High‑Key Mono)");
            frontCamera11.Add("Animoji and Memoji");
            frontCamera11.Add("4K video recording at 24 fps, 30 fps, or 60 fps");
            frontCamera11.Add("1080p HD video recording at 30 fps or 60 fps");
            frontCamera11.Add("Slo‑mo video support for 1080p at 120 fps");
            frontCamera11.Add("Next‑generation Smart HDR for photos");
            frontCamera11.Add("Extended dynamic range for video at 30 fps");
            frontCamera11.Add("Cinematic video stabilization(4K, 1080p, and 720p)");
            frontCamera11.Add("Wide color capture for photos and Live Photos");
            frontCamera11.Add("Retina Flash");
            frontCamera11.Add("Auto image stabilization");
            frontCamera11.Add("Burst mode");

            List<string> frontCamera11Pro = new List<string>();
            frontCamera11Pro.Add("12MP camera");
            frontCamera11Pro.Add("ƒ / 2.2 aperture");
            frontCamera11Pro.Add("Portrait mode with advanced bokeh and Depth Control");
            frontCamera11Pro.Add("Portrait Lighting with six effects(Natural, Studio, Contour, Stage, Stage Mono, High‑Key Mono)");
            frontCamera11Pro.Add("Animoji and Memoji");
            frontCamera11Pro.Add("4K video recording at 24 fps, 30 fps, or 60 fps");
            frontCamera11Pro.Add("1080p HD video recording at 30 fps or 60 fps");
            frontCamera11Pro.Add("Slo‑mo video support for 1080p at 120 fps");
            frontCamera11Pro.Add("Next‑generation Smart HDR for photos");
            frontCamera11Pro.Add("Extended dynamic range for video at 30 fps");
            frontCamera11Pro.Add("Cinematic video stabilization(4K, 1080p, and 720p)");
            frontCamera11Pro.Add("Wide color capture for photos and Live Photos");
            frontCamera11Pro.Add("Retina Flash");
            frontCamera11Pro.Add("Auto image stabilization");
            frontCamera11Pro.Add("Burst mode");

            Dictionary<string, object> iPhoneSE_BodyFeatures = new Dictionary<string, object>();
            iPhoneSE_BodyFeatures.Add("Size and Weight", sizeWeightSE);
            iPhoneSE_BodyFeatures.Add("Display", displaySE);
            iPhoneSE_BodyFeatures.Add("Camera", cameraSE);
            iPhoneSE_BodyFeatures.Add("Video Recording", videoSE);
            iPhoneSE_BodyFeatures.Add("Front Camera", frontCameraSE);

            Dictionary<string, object> iPhone11_BodyFeatures = new Dictionary<string, object>();
            iPhone11_BodyFeatures.Add("Size and Weight", sizeWeight11);
            iPhone11_BodyFeatures.Add("Display", display11);
            iPhone11_BodyFeatures.Add("Camera", camera11);
            iPhone11_BodyFeatures.Add("Video Recording", video11);
            iPhone11_BodyFeatures.Add("Front Camera", frontCamera11);

            Dictionary<string, object> iPhone11Pro_BodyFeatures = new Dictionary<string, object>();
            iPhone11Pro_BodyFeatures.Add("Size and Weight", sizeWeight11Pro);
            iPhone11Pro_BodyFeatures.Add("Display", display11Pro);
            iPhone11Pro_BodyFeatures.Add("Camera", camera11Pro);
            iPhone11Pro_BodyFeatures.Add("Video Recording", video11Pro);
            iPhone11Pro_BodyFeatures.Add("TrueDepth Camera", frontCamera11Pro);

            Dictionary<string, object> iPhone11ProMax_BodyFeatures = new Dictionary<string, object>();
            iPhone11ProMax_BodyFeatures.Add("Size and Weight", sizeWeight11ProMax);
            iPhone11ProMax_BodyFeatures.Add("Display", display11ProMax);
            iPhone11ProMax_BodyFeatures.Add("Camera", camera11Pro);
            iPhone11ProMax_BodyFeatures.Add("Video Recording", video11Pro);
            iPhone11ProMax_BodyFeatures.Add("TrueDepth Camera", frontCamera11Pro);

            iPhoneSE_Features.Add("Body", iPhoneSE_BodyFeatures);
            iPhoneSE_Features.Add("Touch ID", "Fingerprint sensor built into the Home button");
            iPhoneSE_Features.Add("Apple Pay", "Pay with your iPhone using Touch ID in stores, within apps, and on the web");

            iPhone11_Features.Add("Body", iPhone11_BodyFeatures);
            iPhone11_Features.Add("Face ID", "With TrueDepth camera for facial recognition");
            iPhone11_Features.Add("Apple Pay", "Pay with your iPhone using Face ID in stores, within apps, and on the web");

            iPhone11Pro_Features.Add("Body", iPhone11Pro_BodyFeatures);
            iPhone11Pro_Features.Add("Face ID", "With TrueDepth camera for facial recognition");
            iPhone11Pro_Features.Add("Apple Pay", "Pay with your iPhone using Face ID in stores, within apps, and on the web");

            iPhone11ProMax_Features.Add("Body", iPhone11ProMax_BodyFeatures);
            iPhone11ProMax_Features.Add("Face ID", "With TrueDepth camera for facial recognition");
            iPhone11ProMax_Features.Add("Apple Pay", "Pay with your iPhone using Face ID in stores, within apps, and on the web");



            string iPhoneSeFeatureSet = JsonConvert.SerializeObject(iPhoneSE_Features);
            string iPhone11FeatureSet = JsonConvert.SerializeObject(iPhone11_Features);
            string iPhone11ProFeatureSet = JsonConvert.SerializeObject(iPhone11Pro_Features);
            string iPhone11ProMaxFeatureSet = JsonConvert.SerializeObject(iPhone11ProMax_Features);

            iPhoneSE_A.FeatureSet = iPhoneSeFeatureSet;
            iPhoneSE_B.FeatureSet = iPhoneSeFeatureSet;
            iPhoneSE_C.FeatureSet = iPhoneSeFeatureSet;

            iPhone11_A.FeatureSet = iPhone11FeatureSet;
            iPhone11_B.FeatureSet = iPhone11FeatureSet;
            iPhone11_C.FeatureSet = iPhone11FeatureSet;

            iPhone11Pro_A.FeatureSet = iPhone11ProFeatureSet;
            iPhone11Pro_B.FeatureSet = iPhone11ProFeatureSet;
            iPhone11Pro_C.FeatureSet = iPhone11ProFeatureSet;

            iPhone11ProMax_A.FeatureSet = iPhone11ProFeatureSet;
            iPhone11ProMax_B.FeatureSet = iPhone11ProFeatureSet;
            iPhone11ProMax_C.FeatureSet = iPhone11ProFeatureSet;
            #endregion

            // Add Manafacturers
            modelBuilder.Entity<Manufacturer>().HasData(manufacturer);

            // Add Products
            modelBuilder.Entity<Product>().HasData(productA);
            modelBuilder.Entity<Product>().HasData(productB);
            modelBuilder.Entity<Product>().HasData(productC);
            modelBuilder.Entity<Product>().HasData(productD);
            modelBuilder.Entity<Product>().HasData(productE);

            modelBuilder.Entity<Product>().HasData(iPhoneSE_A);
            modelBuilder.Entity<Product>().HasData(iPhoneSE_B);
            modelBuilder.Entity<Product>().HasData(iPhoneSE_C);

            modelBuilder.Entity<Product>().HasData(iPhone11_A);
            modelBuilder.Entity<Product>().HasData(iPhone11_B);
            modelBuilder.Entity<Product>().HasData(iPhone11_C);

            modelBuilder.Entity<Product>().HasData(iPhone11Pro_A);
            modelBuilder.Entity<Product>().HasData(iPhone11Pro_B);
            modelBuilder.Entity<Product>().HasData(iPhone11Pro_C);

            modelBuilder.Entity<Product>().HasData(iPhone11ProMax_A);
            modelBuilder.Entity<Product>().HasData(iPhone11ProMax_B);
            modelBuilder.Entity<Product>().HasData(iPhone11ProMax_C);

            // Add Joins
            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = productA.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = productB.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = productC.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = productD.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = productE.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhoneSE_A.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhoneSE_B.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhoneSE_C.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhone11_A.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhone11_B.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhone11_C.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhone11Pro_A.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhone11Pro_B.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhone11Pro_C.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhone11ProMax_A.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhone11ProMax_B.Id,
                ManufacturerId = manufacturer.Id
            });

            modelBuilder.Entity<ProductManufacturerCatalog>().HasData(new ProductManufacturerCatalog()
            {
                ProductId = iPhone11ProMax_C.Id,
                ManufacturerId = manufacturer.Id
            });
        }
    }
}
