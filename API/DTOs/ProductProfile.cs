﻿using System;
using System.Collections.Generic;
using AutoMapper;

using KomputerStoreAPI.Models;

namespace KomputerStoreAPI.DTOs
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product, ProductDTO>();
            CreateMap<Product, ProductShortDTO>();
        }
    }
}
