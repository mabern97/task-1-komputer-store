﻿using System;
using System.Collections.Generic;

namespace KomputerStoreAPI.DTOs
{
    public class ProductShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }

    public class ProductDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }

        public Dictionary<string, object> Features { get; set; }
    }
}
